export default defineI18nConfig(() => ({
    legacy: false,
    locale: 'en',
    messages: {
        ru: {
            helpTitle: 'Помогаем бизнесу расти через технологии',
            descriptionHelp:`Разработка ERP платформы и CRM-системы, автоматизируем документооборот и бизнес-процессы, внедряем решения для анализа данных, проектируем и сопровождаем ИТ-инфраструктуру. Автоматизация и консалтинг бизнес процессов для компаний`
        },
        en: {
            helpTitle: 'We help businesses grow through technology',
            descriptionHelp: `Development of an ERP platform and CRM system, automating document flow and business processes,  we implement solutions   for data analysis, we design and support IT infrastructure. Automation and consulting of business processes for companies`
        }
    }
}))
