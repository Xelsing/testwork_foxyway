// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    modules: [
        '@element-plus/nuxt',
        '@nuxtjs/i18n',
        '@nuxtjs/tailwindcss',
        'nuxt-swiper'
    ],
    i18n: {
        locales: ['ru', 'en'],  // used in URL path prefix
        defaultLocale: 'ru',    // default locale of your project for Nuxt pages and routings
    },
    devtools: {enabled: true}
})
